/** @type {import('@docusaurus/types').DocusaurusConfig} */
const math = require('remark-math');
const katex = require('rehype-katex');
module.exports = {
  title: 'Robossembler',
  tagline: 'Отворённая Робототехника',
  url: 'https://robossembler.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'robossembler', // Usually your GitHub org/user name.
  projectName: 'robossembler.gitlab.io', // Usually your repo name.
  themeConfig: {
    image: 'img/robofactory-art.jpg',
    navbar: {
      title: 'Robossembler',
      logo: {
        alt: 'Robossembler Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Документация',
          position: 'left',
        },
        {to: 'blog', label: 'Новости', position: 'left'},
        {
          href: 'https://t.me/robossembler_ru',
          label: 'Telegram',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/robossembler',
          label: 'GitLab',
          position: 'right',
        }
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Документация',
          items: [
            {
              label: 'Обзор репозиториев',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Сообщество',
          items: [
            {
              label: 'GitLab-группа',
              href: 'https://gitlab.com/robossembler',
            },
            {
              label: 'Telegram-канал',
              href: 'https://t.me/robossembler_ru',
            },
          ],
        },
        {
          title: 'Подробнее',
          items: [
            {
              label: 'Новости',
              to: 'blog',
            },
          ],
        },
      ],
      copyright: `Copyleft © ${new Date().getFullYear()} Команда Robossembler, сайт сделан при помощи Docusaurus. ООО «Робосборище», ИНН 5024227970, ОГРН 1225000134677`
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/robossembler/robossembler.gitlab.io/-/edit/master/',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/robossembler/robossembler.gitlab.io/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.11/dist/katex.min.css',
      integrity:
        'sha384-Um5gpz1odJg5Z4HAmzPtgZKdTBHZdw8S29IecapCSB31ligYPhHQZMIlWLYQGVoc',
      crossorigin: 'anonymous',
    },
  ],
};
