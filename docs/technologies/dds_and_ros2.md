---
id: dds_and_ros2
title: 'ROS2 и DDS: совместимость движет роботами нового поколения'
---

*Оригинал: [ROS 2 and DDS: Interoperability Drives Next-Generation Robotics, 2020](https://www.rti.com/blog/ros-2-and-dds-interoperability-drives-next-generation-robotics)*

ROS2 уже получила широкое распространение в проектах разработки автомобилей и робототехники. Поскольку сегодняшние новаторы начинают создавать инновации завтрашнего дня, следующим логическим поворотным моментом является развертывание производственного уровня. Но коммерческой робототехнике следующего поколения потребуется нечто большее, чем текущий код ROS чтобы соответствовать необходимым требованиям к производительности, совместимости, безопасности и масштабируемости. И здесь в игру вступает DDS.

Хорошие новости? Во многих отношениях ROS2 уже является приложением Data Distribution Service ™ (DDS). Это означает, что компоненты ROS2 могут свободно смешиваться с собственными компонентами DDS с полной совместимостью. И эта совместимость имеет решающее значение, поскольку правильный подход к проектированию гибридной системы может обеспечить возможности, выходящие далеко за рамки того, что могут предложить только ROS или ROS2.

Хотя DDS является общепризнанным стандартом, важно отметить, что не все предложения DDS созданы одинаково. Теоретически создание приложений на платформе ROS2 может стать отличным введением в то, как среда DDS позволяет создавать высоконадежные распределенные системы. Но на самом деле многим критически важным приложениям требуется больше от DDS, например высокая производительность, больше вариантов QoS или более простая совместимость с компонентами, не относящимися к ROS. Когда производство-класс робототехника является целью приложению ROS2 необходимо полный спектр возможностей , предлагаемых более надежной основой программного обеспечения , такие как RTI Connext ® DDS, особенно если цель состоит в том, чтобы ввести конкурентные, реальные рынки. По сути, Connext DDS расширяет набор инструментов дизайнера.

С технической стороны, как я буду исследовать в этом блоге, достижение этой совместимости и позиционирование ваших усилий по разработке в будущем может быть удивительно простым, потому что ROS2 была построена на DDS и использует стандартную модель данных. С точки зрения DDS, все приложения ROS2 на самом деле являются приложениями DDS. Таким образом, использование одних и тех же типов данных, названий тем и настроек QoS может быть простым делом, чтобы сделать приложение, отличное от ROS2 (например, построенное на Connext DDS), полностью совместимым с существующими приложениями ROS2. В этом сценарии вы также можете, надеюсь, пропустить эти дорогостоящие моменты «вернуться к чертежной доске» и сосредоточиться на своих дизайнерских целях.

## Модель данных ROS2

Несмотря на изменения и улучшения с каждым выпуском ROS и ROS2, основные элементы модели данных ROS остались очень согласованными. К ним относятся определения типов данных для различных датчиков, геометрических форм, траекторий, навигации, визуализации и т.д., определенных как объекты данных и как темы запросов / ответов для удаленных вызовов процедур.

Эти типы данных определены в формате файла, специфичном для ROS, например:

![ros_msg](https://www.rti.com/hubfs/Google%20Drive%20Integration/Blog%20Draft%20ROS2%20IDL%20Interop.png)

Этот формат можно легко преобразовать в стандартные определения типов данных IDL или XML. Это происходит как побочный продукт сборки ROS2 из исходников. Однако файлы IDL несколько разбросаны среди промежуточных файлов, которые создаются при сборке ROS2.

К счастью, есть более простой способ. На сайте  [RTI Community GitHub](https://github.com/rticommunity/ros-data-types)  RTI поддерживает предварительно преобразованный набор файлов IDL модели данных ROS2, упорядоченный так, чтобы его можно было легко построить, включить и использовать в любом приложении, которое должно взаимодействовать с ROS2

## Создание библиотеки поддержки типов ROS2

Этот проект настроен для создания статической библиотеки и файлов заголовков на ваш выбор: C, C ++ (традиционный) или C ++ 11 («Современный C ++»). При этом используется CMake для упрощения процесса сборки и генератор кода RTI, в результате чего код переносится на разные платформы, процессоры и операционные системы / RTOS. Я построил этот пример в Linux; другие хосты также поддерживаются.

Откройте терминал в каталоге для сборки этого проекта, затем введите следующие команды для клонирования и сборки библиотеки:

```shell
git clone https://github.com/rticommunity/ros-data-types.git ros-data-types

source ~/rti_connext_dds-6.0.1/resource/scripts/rtisetenv_x64Linux3gcc5.4.0.bash

cmake -Hros-data-types -Bros-data-types/build \ -DCMAKE_INSTALL_PREFIX=ros-data-types/install -DLANG=C++11

cmake --build ros-data-types/build -- install
```

Этап сборки может занять несколько минут (обратите внимание, что модель данных ROS2 включает более сотни определений типов данных), а результаты будут помещены в ./ros-data-types/install.

Этот каталог содержит:
| Путь | Описание |
| ------ | ------ |
| `install/lib/libRosDataTypes.a` | Статическая библиотека поддержки типа данных DDS для ROS2 |
| `install/include/*` | Дерево заголовочных файлов для использования библиотеки |

## Давайте взаимодействовать

В качестве простого примера взаимодействия давайте создадим небольшую программу, не относящуюся к ROS, которая будет публиковать данные трехмерного облака точек с использованием типа данных ROS2 «PointCloud2» для просмотра с помощью визуализатора «RViz2» в ROS2. Для тех, кто имеет опыт разработки ROS. приложения, написание кода с использованием собственного API Connext C ++ 11 будет выглядеть очень знакомо:

Connext API используется внутри ROS2 при запуске на уровне Connext RMW. Мы просто обходим уровни ROS2, чтобы получить полный доступ к Connext.

RTI теперь предлагает новый уровень Connext RMW, который мы разработали с нуля, чтобы упростить код и значительно повысить производительность, обеспечивая доступ «под ключ» к мощным инструментам разработки распределенных систем RTI (подробности см. В официальном объявлении ).

В этом примере для создания данных облака точек используется один исходный файл. Код очень компактный:

![dds_example](https://www.rti.com/hubfs/Google%20Drive%20Integration/Blog%20Draft%20ROS2%20IDL%20Interop-1.png)

В этом примере используется чистый C ++ 11 API для Connext - без помощи каких - либо добавленных оберток или инкапсуляция - но это по - прежнему очень компактный, очень легко понять, и работает на максимальной производительности из - за отсутствия промежуточных слоев программного обеспечения . Полный набор возможностей Connext доступен напрямую из этого API, и он легко взаимодействует с ROS2.

## Сборка и запуск приложения

В примере используется вспомогательный сценарий RTI CMake, выделенный в предыдущем блоге для упрощения процесса сборки на разных платформах (Linux, Windows и т. Д.), Что приводит к правильно настроенным файлам make / solution для сборки приложения.

На хосте Linux это выглядит так:

```shell
mkdir build; cd build 

cmake -G «Unix Makefiles» .. 

make
```

После того, как приложение построено, его можно запускать непосредственно в терминале:

```shell
build / ptc_demo
```

Чтобы визуализировать данные облака точек, запустите визуализатор ROS2 «RViz2» и с помощью кнопки и вкладки «Add .. by topic» выберите топик PointCloud2 с именем «ptcloud_test». Вы должны увидеть изображение, подобное этому:

![rviz2_with_example](https://www.rti.com/hubfs/Google%20Drive%20Integration/Blog%20Draft%20ROS2%20IDL%20Interop-2.png)

## Вывод

ROS2 предоставляет удобную среду для создания приложений робототехники, но есть много мест, где требуется максимальная производительность и полный набор возможностей, доступных в Connext DDS. Непосредственно используя Connext с моделью данных ROS2, разработчики систем могут получить лучшее из обоих - весь диапазон и производительность Connext И полноценное использование экосистемы ROS2 - эффективно умножая свой набор инструментов для создания полностью оптимизированных систем.

## Ссылки на ресурсы

[Типы данных ROS2 в IDL](https://github.com/rticommunity/ros-data-types)

[Пример кода приложения](https://community.rti.com/ros)

[Уровень Connext RMW](https://github.com/rticommunity/rmw_connextdds/)
