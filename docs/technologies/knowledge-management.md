---
id: knowledge-management
title: 'Управление знаниями'
---

В рамках одного инженерного проекта часто приходится согласовывать между собой множество описаний из различных предметных областей. Описания 3D-моделей, электрических схем, функциональных требований, план-графиков и программ создаются в различных средах, тяжело интегрирующихся между собой. Автоматизация согласования данных через различные форматы экспорта становится затруднительной. В силу сложности решаемой нами задачи мы рассматриваем способы решения этой проблемы.

Одним из таких способов является семантическая интерация с помощью _общей базы знаний в виде графа_ (graph database, knowledge graph, triplestore). Графы обладают мощнейшей выразительной силой, позволяющей соединить разнообразные терминологии в рамках единой модели. Инструменты работы с графами знаний могут выявлять противоречия и несоответствия между данными, в результате чего достигается согласованное состояние, из которого можно получить артефакты в виде конфигурационных файлов или даже генерировать программы.

## Сборник ссылок по Semantic Web

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=semantalytics&repo=awesome-semantic-web&show_owner=true)](https://github.com/semantalytics/awesome-semantic-web)

![](https://img.shields.io/github/contributors/semantalytics/awesome-semantic-web.svg)
![](https://img.shields.io/github/last-commit/semantalytics/awesome-semantic-web.svg)
![](https://img.shields.io/github/commit-activity/m/semantalytics/awesome-semantic-web.svg)


## Графовые базы данных и хранилища триплетов

Выбраны из [списка](https://github.com/semantalytics/awesome-semantic-web#databases)  по критериям:
* Open Source
* Активность разработки в 2021 году
* Минимум зависимостей
* Атомарные отслеживаемые изменения (git-подобность)
* Количество контрибьюторов (Atomic Data - исключение)

### Quit Store

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=AKSW&repo=QuitStore&show_owner=true)](https://github.com/AKSW/QuitStore)

![](https://img.shields.io/github/contributors/AKSW/QuitStore.svg)
![](https://img.shields.io/github/last-commit/AKSW/QuitStore.svg)
![](https://img.shields.io/github/commit-activity/m/AKSW/QuitStore.svg)

Quads in Git - Distributed Version Control for RDF Knowledge Bases. The Quit Store (stands for Quads in Git) provides a workspace for distributed collaborative Linked Data knowledge engineering. You are able to read and write RDF Datasets (aka. multiple Named Graphs) through a standard SPARQL 1.1 Query and Update interface. To collaborate you can create multiple branches of the Dataset and share your repository with your collaborators as you know it from Git.

* Paper: https://natanael.arndt.xyz/bib/arndt-n-2018--jws

### Fluree DB

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=fluree&repo=db&show_owner=true)](https://github.com/fluree/db)

![](https://img.shields.io/github/contributors/fluree/db.svg)
![](https://img.shields.io/github/last-commit/fluree/db.svg)
![](https://img.shields.io/github/commit-activity/m/fluree/db.svg)

Fluree is an immutable, temporal, ledger-backed semantic graph database that has a cloud-native architecture.

* Документация: https://developers.flur.ee/docs/

### Atomic Data

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=joepio&repo=atomic-data-rust&show_owner=true)](https://github.com/joepio/atomic-data-rust)

![](https://img.shields.io/github/contributors/joepio/atomic-data-rust.svg)
![](https://img.shields.io/github/last-commit/joepio/atomic-data-rust.svg)
![](https://img.shields.io/github/commit-activity/m/joepio/atomic-data-rust.svg)

* Документация: https://docs.atomicdata.dev/
* Демо: https://atomicdata.dev/
* GUI: https://github.com/joepio/atomic-data-browser

Atomic Data is a modular specification for sharing, modifying and modeling graph data. It combines the ease of use of JSON, the connectivity of RDF (linked data) and the reliability of type-safety. Atomic Data uses links to connect pieces of data, and therefore makes it easier to connect datasets to each other - even when these datasets exist on separate machines. Atomic Data is especially suitable for knowledge graphs, distributed datasets, semantic data, p2p applications, decentralized apps and linked open data. It is designed to be highly extensible, easy to use, and to make the process of domain specific standardization as simple as possible.

### LevelGraph

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=levelgraph&repo=levelgraph&show_owner=true)](https://github.com/levelgraph/levelgraph)

![](https://img.shields.io/github/contributors/levelgraph/levelgraph.svg)
![](https://img.shields.io/github/last-commit/levelgraph/levelgraph.svg)
![](https://img.shields.io/github/commit-activity/m/levelgraph/levelgraph.svg)

### gStore

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=pkumod&repo=gStore&show_owner=true)](https://github.com/pkumod/gStore)

![](https://img.shields.io/github/contributors/pkumod/gStore.svg)
![](https://img.shields.io/github/last-commit/pkumod/gStore.svg)
![](https://img.shields.io/github/commit-activity/m/pkumod/gStore.svg)

Gstore System(also called gStore) is a graph database engine for managing large graph-structured data, which is open-source and targets at Linux operation systems. The whole project is written in C++, with the help of some libraries such as readline, antlr, and so on. Only source tarballs are provided currently, which means you have to compile the source code if you want to use our system.

### Oxigraph

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=oxigraph&repo=oxigraph&show_owner=true)](https://github.com/oxigraph/oxigraph)

![](https://img.shields.io/github/contributors/oxigraph/oxigraph.svg)
![](https://img.shields.io/github/last-commit/oxigraph/oxigraph.svg)
![](https://img.shields.io/github/commit-activity/m/oxigraph/oxigraph.svg)

Oxigraph is a graph database implementing the SPARQL standard. Its goal is to provide a compliant, safe, and fast graph database based on the RocksDB key-value store. It is written in Rust. It also provides a set of utility functions for reading, writing, and processing RDF files.


### QUADSTORE

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=beautifulinteractions&repo=node-quadstore&show_owner=true)](https://github.com/beautifulinteractions/node-quadstore)

![](https://img.shields.io/github/contributors/beautifulinteractions/node-quadstore.svg)
![](https://img.shields.io/github/last-commit/beautifulinteractions/node-quadstore.svg)
![](https://img.shields.io/github/commit-activity/m/beautifulinteractions/node-quadstore.svg)

Quadstore is a LevelDB-backed RDF graph database for Node.js and the browser with native support for quads and querying across named graphs, RDF/JS interfaces and SPARQL queries.

### Terminus DB

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=terminusdb&repo=terminusdb&show_owner=true)](https://github.com/terminusdb/terminusdb)

![](https://img.shields.io/github/contributors/terminusdb/terminusdb.svg)
![](https://img.shields.io/github/last-commit/terminusdb/terminusdb.svg)
![](https://img.shields.io/github/commit-activity/m/terminusdb/terminusdb.svg)

TerminusDB is an open-source graph database and document store. TerminusDB allows you to link JSON documents in a powerful knowledge graph all through a simple document API. Use TerminusDB to enable Git-like collaborative workflows in your application.

Нужно разобраться какие есть ограничения у бесплатной версии. Оно вроде open source, но в платных тарифах смущают следующие опции:
* Project collaboration
* Unlimited metadata & data sources
* API Access
* Cryptographically Sealed

### Gun

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=amark&repo=gun&show_owner=true)](https://github.com/amark/gun)

![](https://img.shields.io/github/contributors/amark/gun.svg)
![](https://img.shields.io/github/last-commit/amark/gun.svg)
![](https://img.shields.io/github/commit-activity/m/amark/gun.svg)

An open source cybersecurity protocol for syncing decentralized graph data.