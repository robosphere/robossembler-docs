---
id: multi-agent-planning
title: 'Многоагентное планирование'
---

## Проекты

* [libMultiRobotPlanning](https://github.com/whoenig/libMultiRobotPlanning) - Библиотека с поисковыми алгоритмами для планирования задач и маршрутов движения у систем, состоящих из многих роботов/агентов
* [pymapf](https://github.com/APLA-Toolbox/pymapf) - Набор python-утилит для много-агентного планирования (цетрализованного и децентрализованного)
* [multi_agent_path_planning](https://github.com/atb033/multi_agent_path_planning) - имплементации некоторых алгоритмов много-агентного поиска пути на Python
* [DOMAP](https://github.com/smart-pucrs/DOMAP)(The Distributed Online Multi-Agent Planning framework). Фреймворк предоставляет возможности для решения задач много-агентного планирования online.
	Состоит из трёх модулей:
	* `goal allocation mechanism` - используется для распределения целей между агентами;
	* `individual planner` - используется на этапе индивидуального планирования каждого агента;
	* `coordination mechanism` - используется до или после планирования, чтобы избежать возможных конфликтов, которые могут возникнуть во время планирования. DOMAP имплементирован в платформе [JaCaMo](http://jacamo.sourceforge.net/).
* [RMF](https://osrf.github.io/ros2multirobotbook/rmf-core.html) - Robot Middleware Framework - система управления трафиком для много-агентных систем в ROS2.