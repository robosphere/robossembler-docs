---
id: photopolymer
title: Фотополимерная печать
---

### Алексей

Мы при некотором желании сможем построить дешевый фотополимерный принтер. С учетом того,  что стоимость литра фотополимера 1500 руб.  (не медицинского). С использованием головы от обычного струйного принтера и либо ультрофиолетового светодиода, либо попробовать приминить мощное электростатическое поле.

Что то вроде этого. У моего друга был подобный. Очень качественно печатал
https://top3dshop.ru/kupit-3d-printer/prof/stratasys-objet-24.html

Хорошее описание
https://technoprist.ru/catalog/3d-printery/3d-printery-fotopolimer/stratasys-connex1-objet260/

Пример качества печати
https://im0-tub-ru.yandex.net/i?id=7cc6fc7ae84247dc96745425450205b7-l&n=13

Этот принтер не конкурент в силу его адской цены. Даже самый дешевый бу стоит за 300 000 . а цена принтера по крупнее и нового это миллионы рублей. И он очено не удобен и дорог в использовании. Работает только с подключением инета, картриджи самовыключаются ести просрочен срок. Стоят как чугунный мост. Мы можем сделать подобное изделие

https://www.avito.ru/moskva/orgtehnika_i_rashodniki/3d_printer_stratasys_objet30_pro_1810224106?utm_campaign=native&utm_medium=item_page_android&utm_source=soc_sharing

Я скорее всего его голову рассматриваю, как дополнение к станку. Со сменными насадками. Но качество печали получается отменным,  почти как литье. Бу 360 000. Скорее всего без картриджей. Новый комплектный стоил 1 500 000руб

### Станислав

В подобную стоимость можно впихнуть ТПА с технологией MIM и ЧПУ со сменой инструмента для производства пресс-форм. Только отливка таких деталей будет занимать секунды. Но есть "но" во всех технологиях...

### Александр

Фотополимерная технология очень сильна для робо фабрики. Минус её в том что ей нелегко напечатать объем больший чем принтер, что легко для fdm в руке РМ. Из плюсов - удобство синтеза и смешивания жидкостей. В рамках экстремального low tech воспроизводства уф лазер можно сделать на воздухе из почти ничего. Но по хорошему нужен Фемтосекундный лазер, он печатает не в слое а в объёме с огромной точностью.

* https://physicsopenlab.org/2020/07/16/diy-nitrogen-tea-laser/
* http://nanoscribe.de

И экструзионная и фото технологии печати самодлстаточны для робо фабрики. Но при наличии обоих её мощь несомненно будет выше. Мы можем вместо современных процессоров строить большие ЭВМ но воспроизводимые силой фабрики. Например модули памяти и процессор макро размерные, соединяемые при помощи РМ. Даже если такие ЭВМ займут половину фабрики, самовоспроизводимость делает это рациональным.