# Инструкция по установке фреймворка

## Шаг 1: Установка ROS2 Humble  

Для начала установите [ROS2 Humble](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debs.html).  
Рекомендуется минимальная установка `ros-humble-ros-base`, а также пакет `ros-dev-tools`.

## Шаг 2: Проверка среды ROS2  

Перед продолжением убедитесь, что среда ROS2 активирована. Для этого выполните:
```sh
source /opt/ros/humble/setup.bash
```

## Шаг 3: Если Вы этого не делали то сделайте
```sh
sudo rosdep init
rosdep update
```

## Шаг 4: Установка фреймворка

```sh
cd 
mkdir -p robossembler-ws/src && cd robossembler-ws/src
git clone --recurse-submodules https://gitlab.com/robossembler/robossembler-ros2.git
```

Далее необходимо собрать [`ros2_control`](https://github.com/ros-controls/ros2_control) из исходников, используя этот [форк](https://github.com/solid-sinusoid/ros2_control/tree/gz-ros2-cartesian-controllers).  
Также доступна альтернатива с использованием [`vsctool`](https://github.com/dirk-thomas/vcstool), который входит в состав базовых пакетов ROS2.

Если вы решили использовать `vcstool`, нужные пакеты будут клонированы в тоже рабочее пространство, что и сам фреймворк. Команда для этого выглядит следующим образом:
```sh
vcs import . < robossembler-ros2/repos/all-deps.repos
```

Вы также можете установить все необходимые библиотеки Python, выполнив команду:
```shell
pip install -r robossembler-ros2/repos/requirements.txt
# Если Вы получили ошибку с установкой Shapely
sudo apt install libgeos-dev
```

> **[!ВНИМАНИЕ]**  
> Убедитесь, что у вас установлен `git lfs`. В файле `requirements.txt` указан модуль `rbs_assets_library`, который содержит большие файлы и устанавливается как Python-модуль.

Эти команды нужно выполнять в директории `robossembler-ws/src/`.

Установка зависимостей при помощи `rosdep`
```sh
cd ~/robossembler-ws
rosdep install --from-paths src -y --ignore-src --rosdistro ${ROS_DISTRO}
```

Сборка фреймворка при помощи `colcon`
```sh
colcon build
```

## Полная последовательность команд  

Ниже приведён полный набор команд для настройки фреймворка:
```sh
cd 
mkdir -p robossembler-ws/src && cd robossembler-ws/src
git clone --recurse-submodules https://gitlab.com/robossembler/robossembler-ros2.git
# Или, если вы предпочитаете Radicle:
git clone --recurse-submodules https://seed.robossembler.org/z46gtVRpXaXrGQM7Fxiqu7pLy7kip.git robossembler-ros2
rad clone rad:z46gtVRpXaXrGQM7Fxiqu7pLy7kip

vcs import . < robossembler-ros2/repos/all-deps.repos
pip install -r robossembler-ros2/repos/requirements.txt
cd ..
rosdep install --from-paths src -y --ignore-src --rosdistro ${ROS_DISTRO}
colcon build
```

