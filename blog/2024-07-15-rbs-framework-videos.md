---
slug: rbs-framework-videos
title: Видео-демонстрации работы программных модулей Фреймворка Робосборщик
author: Игорь Брылёв
author_title: Team Lead @ Robossembler
author_url: https://gitlab.com/movefasta
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/4249760/avatar.png
tags: [robossembler, milestone, summary]
---

Краткие ознакомительные видео-демонстрации работы программных модулей Robossembler Framework (https://robossembler.org/docs/robossembler-framework/), который сейчас разрабатывается нашей командой.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OR-pSsiaEF0?si=EYauUCESIGjsND0L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
