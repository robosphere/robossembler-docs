module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Общая информация',
      collapsed: false,
      items: [
        'motivation',
        'robossembler-overview',
        'vacancies'
      ],
    },
    {
      type: 'category',
      label: 'Аппаратное обеспечение',
      items: [
        {
          type: 'link',
          href: 'https://gitlab.com/robossembler/servo',
          label: 'Сервопривод',
        },
        {
          type: 'link',
          href: 'https://gitlab.com/robossembler/roboarm-diy-version',
          label: 'Робот-манипулятор',
        },
        {
          type: 'link',
          href: 'https://gitlab.com/robossembler/cnc/motor-wire-winder',
          label: 'Станок намотки',
        },
      ],
    },
    {
      type: 'category',
      label: 'Программное обеспечение',
      items: [
        'robossembler-framework',
        {
          type: 'doc',
          id: 'technologies/ASP-overview',
          label: 'Генерация последовательности сборки',
        },
        {
          type: 'doc',
          id: 'technologies/cad-cg-pipeline',
          label: 'Экспорт моделей в виртуальные среды',
        },
        {
          type: 'doc',
          id: 'technologies/plansys2',
          label: 'Генерация технологических карт',
        },
        'software/dataset-generator',
        'software/environment-manager',
        {
          type: 'category',
          label: 'Модуль исполнения планов',
          items: [
            {
              type: 'doc',
              id: 'software/ros2',
              label: 'Архитектура',
            },
            {
              type: 'doc',
              id: 'software/ros2/installation',
              label: 'Установка',
            },
            {
              type: 'doc',
              id: 'software/ros2/add_new_robot',
              label: 'Добавление нового робота',
            },
            {
              type: 'doc',
              id: 'software/ros2/prepare-and-execute-skill',
              label: 'Создание и запуск навыка',
            },
          ],
        },
        {
          type: 'doc',
          id: 'software/webservice',
          label: 'Веб-интерфейс',
        },
        {
          type: 'doc',
          id: 'software/usecases',
          label: 'Сценарии использования',
        },
      ],
    },
/*     {
      type: 'category',
      label: 'Прикладные решения',
      items: [
        'applications/beehive',
        'applications/microalgae-garden',
        'applications/sprouts-garden',
        'applications/vegetable-garden'
      ],
    }, */
    {
      type: 'category',
      label: 'Переводы статей, обзоры',
      items: [
        'technologies/cv-perception-methods',
        'technologies/wrs2020-assembly-challenge',
        'papers/mania-beetz-self-training-with-vr-2019',
        'technologies/dds_and_ros2',
        'papers/self-organization-in-robotic-welding',
        'papers/smerobotics',
        'papers/auto-assembly',
        'technologies/mrs-robotics-assembly-review',
        'technologies/machine-learning-in-robotics',
      ],
    },
    {
      type: 'category',
      label: 'Самовоспроизводство техники',
      items: [
        {
          type: 'doc',
          id: 'replication',
          label: 'Дорожная карта САС',
        },
        'analogy',
        'mining',
        {
          type: 'category',
          label: 'Модели',
          items: [
            'models/generation/generation',
            'models/growth/growth'
          ],
        },
        {
          type: 'category',
          label: 'Технические решения',
          items: [
            'technologies/robonomics',
            'autostorage',
            'information/information_support',
            'information/planner',
            'technologies/wood',
            {
              type: 'doc',
              id: 'technologies/recycling',
              label: 'Мусор как сырьё',
            },
          ],
        },
        'glossary',
      ],
    },
    {
      type: 'category',
      label: 'Разное',
      items: [
        'technologies/open-source-robots-and-tools',
        'concept/engelmeyer',
        'workflow-rules',
        'technologies/knowledge-management',
        'information/cfs-models-pub-in-nix'
      ],
    },
  ],
};
