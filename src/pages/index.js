import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Роботы-манипуляторы',
    imageUrl: 'img/robossembler-arm.png',
    description: (
      <>
        <ul>
          <li><a href="https://gitlab.com/robossembler/roboarm-diy-version">Robossembler Arm</a> - 6-DoF манипулятор для изготовления с помощью 3D-печати</li>
          <li><a href="https://gitlab.com/robossembler/roboarm">Манипулятор</a> - 6-DoF для изготовления с помощью литья в <a href="https://gitlab.com/robossembler/cnc/roboarm-link-mold" target="_blank" rel="noopener noreferrer">прессформе</a></li>
        </ul>
      </>
    ),
  },
  {
    title: 'Приспособления',
    imageUrl: 'img/grip-tool.png',
    description: (
      <>
        <ul>
          <li><a href="https://gitlab.com/robossembler/arm-tools/grip-tool" target="_blank" rel="noopener noreferrer">Механический захват</a> <i>прототипируется</i></li>
          <li><a href="https://gitlab.com/robossembler/arm-tools/3d-print-tool" target="_blank" rel="noopener noreferrer">3D-печать</a>, <a href="https://gitlab.com/robossembler/arm-tools/extrude-melt-tool" target="_blank" rel="noopener noreferrer">Подача компаунда</a>, <a href="https://gitlab.com/robossembler/arm-tools/soldering-tool" target="_blank" rel="noopener noreferrer">Пайка</a>, <a href="https://gitlab.com/robossembler/arm-tools/welding-tool" target="_blank" rel="noopener noreferrer">Сварка</a>, <a href="https://gitlab.com/robossembler/arm-tools/post-processing-tool" target="_blank" rel="noopener noreferrer">Фрезерная и пост-обработка</a>, <a href="https://gitlab.com/robossembler/arm-tools/scan-tool" target="_blank" rel="noopener noreferrer">Сканирование и съёмка</a> <i>в разработке</i></li>
        </ul>
      </>
    ),
  },
  {
    title: 'Серводвигатели',
    imageUrl: 'img/motor.png',
    description: (
      <>
        Оригинальные модели серводвигателей, ориентированные для автоматического производства
        <ul>
          <li><a href="https://gitlab.com/robossembler/roboarm-diy-version/-/tree/main/src/MOTOR" target="_blank" rel="noopener noreferrer">Сервопривод</a> для Robossembler Arm</li>
          <li><a href="https://gitlab.com/robossembler/servo" target="_blank" rel="noopener noreferrer">Сервопривод</a> для литьевого манипулятора</li>
        </ul>
      </>
    ),
  },
  {
    title: 'Производственные модули',
    imageUrl: 'img/workspace.png',
    description: (
      <>
        Новые способы масштабирования роботизированных ячеек
        <li><a href="https://gitlab.com/robossembler/cnc/cubic-modular-workspace" target="_blank" rel="noopener noreferrer">Cubic</a> - ячейки на базе кубов</li>
        <li><a href="https://gitlab.com/robossembler/cnc/roboarm-workspace" target="_blank" rel="noopener noreferrer">Hexagonal</a> - ячейки на базе плоских шестигранников</li>
      </>
    ),
  },
  {
    title: 'Прикладное ПО',
    imageUrl: 'img/ros2.jpg',
    description: (
      <>
        Разработка программного обеспечения с открытым исходным кодом для управления роботами
        <ul>
          <li><a href="docs/robossembler-framework/" target="_blank" rel="noopener noreferrer">Robossembler Framework</a> - комплекс ПО для автоматизации сборки произвольных изделий роботами-манипуляторами</li>
        </ul>
      </>
    ),
  },
  {
    title: 'Инструменты поддержки CAD',
    imageUrl: 'img/freecad.svg',
    description: (
      <>
        Плагины к открытым системам проектирования для автоматизации разработки и производства
        <ul>
          <li><a href="https://gitlab.com/robossembler/forks/ARBench" target="_blank" rel="noopener noreferrer">ARBench</a> - плагин FreeCAD для подготовки изделий к роботизированной сборке</li>
        </ul>
      </>
    ),
  },
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Промышленная робототехника с открытыми конструкторской документацией и исходным кодом">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Обзор репозиториев
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
